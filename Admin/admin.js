import {
  hienThiThongTin,
  layThongTinTuForm,
  onSuccess,
  renderProductList,
} from "./controller.js";

const BASE_URL = "https://63f85e4e5b0e4a127de45517.mockapi.io";
let fetchProductList = () => {
  axios({
    url: `${BASE_URL}/QLSP`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      renderProductList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProductList();
window.deleteProduct = (id) => {
  axios({
    url: `${BASE_URL}/QLSP/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res.data);
      onSuccess("Xóa thành công");
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.addProduct = () => {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/QLSP`,
    method: "POST",
    data,
  })
    .then((res) => {
      console.log(res.data);
      onSuccess("Thêm thành công");
      $("#exampleModal").modal("hide");
      fetchProductList();
      resetForm();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.editProduct = (id) => {
  axios({
    url: `${BASE_URL}/QLSP/${id}`,
    method: "GET",
  })
    .then((res) => {
      hienThiThongTin(res.data);
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.updateProduct = () => {
  let data = layThongTinTuForm();
  console.log("data: ", data);
  axios({
    url: `${BASE_URL}/QLSP/${data.id}`,
    method: "PUT",
    data,
  })
    .then((res) => {
      console.log(res.data);
      onSuccess("Cập nhật thành công");
      $("#exampleModal").modal("hide");
      fetchProductList();
      resetForm();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.resetForm = () => {
  document.getElementById("formProduct").reset();
};

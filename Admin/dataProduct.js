[
  {
    name: "THEFOUR Melting Earth T-Shirt WHITE",
    price: 329000,
    img: "../img/2.jpg",
    des: "Công nghệ in kĩ thuật số, thoải mái, mát mẻ",
    type: "white",
    id: "1",
  },
  {
    name: "THEFOUR Melting Earth T-Shirt BLACK",
    price: 329000,
    img: "../img/4.jpg",
    des: "Công nghệ in kĩ thuật số, thoải mái, mát mẻ",
    type: "Black",
    id: "2",
  },
  {
    name: "THEFOUR Rainbow T-Shirt WHITE",
    price: 329000,
    img: "../img/11.png",
    des: "Công nghệ in kĩ thuật số, thoải mái, mát mẻ",
    type: "White",
    id: "3",
  },
  {
    name: "THEFOUR Rainbow T-Shirt BLACK",
    price: 329000,
    img: "../img/10.png",
    des: "Công nghệ in kĩ thuật số, thoải mái, mát mẻ",
    type: "Black",
    id: "4",
  },
];

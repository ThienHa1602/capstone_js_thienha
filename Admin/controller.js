export let renderProductList = (productArr) => {
  let contentHTML = "";
  productArr.forEach((item) => {
    let contentTr = `
    <tr>
<td>${item.id}</td>
<td>${item.name}</td>
<td>${item.price}</td>
<td><img src="${item.img}" alt=""></td>
<td>${item.des}</td>
<td class="" style="text-align: center"><button class="btn my-3 me-1" data-toggle="modal" data-target="#exampleModal" id="btnEdit" onclick="editProduct(${item.id})">
    Edit<i class="fa fa-pencil-square ms-2"></i>
    </button>
    <button class="btn "  id="btnDelete" onclick="deleteProduct(${item.id})">
    Delete <i class="fa fa-trash ms-2"></i>
    </button></td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("tableProduct").innerHTML = contentHTML;
};
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
export let layThongTinTuForm = () => {
  let id = document.getElementById("maSo").value;
  let name = document.getElementById("name").value;
  let price = document.getElementById("price").value * 1;
  let img = document.getElementById("img").value;
  let desc = document.getElementById("desc").value;
  let type = document.getElementById("type").value;
  return { id, name, price, img, desc, type };
};
export let hienThiThongTin = (item) => {
  document.getElementById("maSo").value = item.id;
  document.getElementById("name").value = item.name;
  document.getElementById("price").value = item.price;
  document.getElementById("img").value = item.img;
  document.getElementById("desc").value = item.desc;
  document.getElementById("type").value = item.type ? "White" : "Black";
};

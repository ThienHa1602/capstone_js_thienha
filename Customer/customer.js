import {
  onSuccess,
  renderCart,
  renderProductList,
  valueType,
} from "./controller.js";

const BASE_URL = "https://63f85e4e5b0e4a127de45517.mockapi.io";

let fetchProductList = () => {
  axios({
    url: `${BASE_URL}/QLSP`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      renderProductList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProductList();
window.searchByType = () => {
  axios({
    url: `${BASE_URL}/QLSP`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      renderProductList(valueType(res.data));
    })
    .catch((err) => {
      console.log(err);
    });
};
let cart = [];
window.addToCart = (id, img, price) => {
  let product = {
    id,
    img,
    price,
  };
  var index = cart.findIndex((item) => {
    return item.id == product.id;
  });
  if (index == -1) {
    let newProduct = { ...product, soLuong: 1 };
    cart.push(newProduct);
    console.log("product: ", product);
  } else {
    cart[index].soLuong++;
  }
  renderCart(cart);
};

window.changeQuantity = (id, luaChon) => {
  console.log("cart: ", cart);

  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  cart[index].soLuong = cart[index].soLuong + luaChon;
  cart[index].soLuong == 0 && cart.splice(index, 1);
  renderCart(cart);
};
window.deteleCartItem = (id) => {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  cart.splice(index, 1);
  renderCart(cart);
};
window.purchase = () => {
  onSuccess("Thanh toán thành công");
  $("#exampleModal").modal("hide");
  cart.splice(0, cart.length);
  renderCart(cart);
};
